# Jeu de cartes

Ce projet Java implémente un jeu de cartes .

Un joueur tire une main de 10 cartes de manière aléatoire.
Chaque carte possède une couleur ("Carreaux", par exemple) et une valeur ("10", par exemple).

Le jeu permet de construire un ordre aléatoire des couleurs
, un ordre aléatoire des valeurs
, de fournir une main de 10 cartes non triée et une main triée .

## Prérequis

Avant de commencer, assurez-vous d'avoir les outils suivants installés :

Java JDK 17 ou supérieur

Apache Maven

Un IDE Java (comme IntelliJ IDEA, Eclipse, ou VSCode)

## Installation

1. Clonez ce dépôt sur votre machine locale :

```bash
git clone https://gitlab.com/prolebabs/carte.git
```

Accédez au répertoire du projet :
```bash
cd carte
```
## Exécution
Vous pouvez exécuter l'application de différentes manières :

À l'aide de Maven
Utilisez Maven pour construire et exécuter l'application :

```bash
mvn spring-boot:run
```
À l'aide d'un IDE
Importez le projet dans votre IDE Java préféré et exécutez-le en tant qu'application Spring Boot.

## Utilisation
Une fois l'application en cours d'exécution, vous pouvez accéder à l'API REST pour interagir avec le jeu de cartes.

Par exemple, vous pouvez accéder à l'endpoint  /api/carte pour obtenir un nouveau jeu de cartes.
