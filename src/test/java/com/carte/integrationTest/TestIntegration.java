package com.carte.integrationTest;

import com.carte.record.Carte;
import com.carte.service.ICarteService;
import com.carte.service.IGenerateurCouleur;
import com.carte.service.IGenerateurValeur;
import com.carte.service.impl.CarteService;
import com.carte.service.impl.GenerateurCouleur;
import com.carte.service.impl.GenerateurValeur;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestIntegration {

    @Value("${server.port}")
    private int port;
    private final IGenerateurCouleur generateurCouleur = new GenerateurCouleur();
    private final IGenerateurValeur generateurValeur = new GenerateurValeur();
    private final ICarteService carteService = new CarteService(generateurCouleur, generateurValeur);

    @Test
    public void testCartesSansTri() {
        List<Carte> cartes = given()
                .port(port)
                .when()
                .get("/api/carte")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList(".", Carte.class);
        Assertions.assertEquals(cartes.size(), 10);
    }

    @Test
    public void testCartesAvecTri() {
        List<Carte> carteList = carteService.tirerMain();
        List<Carte> cartes = given()
                .port(port)
                .contentType("application/json")
                .body(carteList)
                .when()
                .post("/api/carte")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList(".", Carte.class);
        Assertions.assertEquals(cartes.size(), 10);
    }
}
