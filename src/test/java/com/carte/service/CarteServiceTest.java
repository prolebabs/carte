package com.carte.service;

import com.carte.record.Carte;
import com.carte.service.impl.CarteService;
import com.carte.service.impl.GenerateurCouleur;
import com.carte.service.impl.GenerateurValeur;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class CarteServiceTest {
    private final IGenerateurCouleur generateurCouleur = new GenerateurCouleur();
    private final IGenerateurValeur generateurValeur = new GenerateurValeur();
    private final ICarteService carteService = new CarteService(generateurCouleur, generateurValeur);

    @Test
    void testTirerMain() {
        Assertions.assertEquals(10, carteService.tirerMain().size());
    }

    @Test
    void testTrierMain() {
        List<Carte> mainTriee = carteService.trierMain(carteService.tirerMain());
        Assertions.assertEquals(10,mainTriee.size());
        for (int i = 1; i < mainTriee.size(); i++) {
            Carte precedent = mainTriee.get(i - 1);
            Carte courant = mainTriee.get(i);
            Assertions.assertTrue(courant.couleur().compareTo(precedent.couleur()) >= 0);
        }
    }
}