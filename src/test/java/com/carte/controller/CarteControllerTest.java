package com.carte.controller;

import com.carte.record.Carte;
import com.carte.service.ICarteService;
import com.carte.service.IGenerateurCouleur;
import com.carte.service.IGenerateurValeur;
import com.carte.service.impl.CarteService;
import com.carte.service.impl.GenerateurCouleur;
import com.carte.service.impl.GenerateurValeur;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;

class CarteControllerTest {
    private final IGenerateurCouleur generateurCouleur = new GenerateurCouleur();
    private final IGenerateurValeur generateurValeur = new GenerateurValeur();
    private final ICarteService carteService = new CarteService(generateurCouleur, generateurValeur);
    private final CarteController carteController = new CarteController(carteService);

    @Test
    void testGetCartesSansTrie() {
        ResponseEntity<List<Carte>> cartes = carteController.getCartes();
        Assertions.assertEquals(200, cartes.getStatusCode().value());
        Assertions.assertEquals(10, cartes.getBody().size());
    }

    @Test
    void testGetCartesAvecTrie() {
        ResponseEntity<List<Carte>> response = carteController.trierCartes(carteService.tirerMain());
        Assertions.assertEquals(200, response.getStatusCode().value());
        List<Carte> cartes = response.getBody();
        Assertions.assertEquals(10, cartes.size());
        for (int i = 1; i < cartes.size(); i++) {
            Carte precedent = cartes.get(i - 1);
            Carte courant = cartes.get(i);
            Assertions.assertTrue(courant.couleur().compareTo(precedent.couleur()) >= 0);
        }
    }
}