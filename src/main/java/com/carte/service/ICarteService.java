package com.carte.service;

import com.carte.record.Carte;

import java.util.List;

public interface ICarteService {

    /**
     * Methode permettant de tirer une main de 10 cartes de manière aléatoire.
     *
     * @return une main de 10 cartes tirée
     */
    List<Carte> tirerMain();

    /**
     * Methode permettant de trier une main de carte prise en parametre
     *
     * @param main une main de carte
     * @return la main de cartes en parametre triée
     */
    List<Carte> trierMain(List<Carte> main);
}
