package com.carte.service.impl;

import com.carte.enums.CouleurCarte;
import com.carte.enums.ValeurCarte;
import com.carte.record.Carte;
import com.carte.service.ICarteService;
import com.carte.service.IGenerateurCouleur;
import com.carte.service.IGenerateurValeur;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

@Service
public class CarteService implements ICarteService {
    private static final int TAILLE_MAIN = 10;
    private final IGenerateurCouleur generateurCouleur;

    private final IGenerateurValeur generateurValeur;
    private final Random random = new Random();

    public CarteService(IGenerateurCouleur generateurCouleur, IGenerateurValeur generateurValeur) {
        this.generateurCouleur = generateurCouleur;
        this.generateurValeur = generateurValeur;
    }

    @Override
    public List<Carte> tirerMain() {
        List<CouleurCarte> couleurCartes = generateurCouleur.getCouleurs();
        List<ValeurCarte> valeurCartes = generateurValeur.getValeurs();
        List<Carte> main = new ArrayList<>();
        for (int i = 0; i < TAILLE_MAIN; i++) {
            Carte carte = new Carte(
                    couleurCartes.get(random.nextInt(couleurCartes.size())),
                    valeurCartes.get(random.nextInt(valeurCartes.size())).getValeur()
            );
            main.add(carte);
        }
        return main;
    }

    @Override
    public List<Carte> trierMain(List<Carte> main) {
        main.sort(Comparator.comparing(Carte::couleur).thenComparingInt(Carte::valeur));
        return main;
    }
}