package com.carte.service.impl;

import com.carte.enums.ValeurCarte;
import com.carte.service.IGenerateurValeur;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class GenerateurValeur implements IGenerateurValeur {
    @Override
    public List<ValeurCarte> getValeurs() {
        List<ValeurCarte> valeurs = new ArrayList<>(List.of(ValeurCarte.values()));
        Collections.shuffle(valeurs);
        return valeurs;
    }
}
