package com.carte.service.impl;

import com.carte.enums.CouleurCarte;
import com.carte.service.IGenerateurCouleur;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class GenerateurCouleur implements IGenerateurCouleur {
    @Override
    public List<CouleurCarte> getCouleurs() {
        List<CouleurCarte> couleurs = new ArrayList<>(List.of(CouleurCarte.values()));
        Collections.shuffle(couleurs);
        return couleurs;
    }
}
