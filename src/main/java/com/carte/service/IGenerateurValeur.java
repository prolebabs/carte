package com.carte.service;

import com.carte.enums.ValeurCarte;

import java.util.List;

public interface IGenerateurValeur {

    /**
     * Méthode pour construire un ordre aléatoire des valeurs.
     *
     * @return Les valeurs.
     */
    List<ValeurCarte> getValeurs();
}
