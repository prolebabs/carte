package com.carte.service;

import com.carte.enums.CouleurCarte;

import java.util.List;

public interface IGenerateurCouleur {

    /**
     * Méthode pour construire un ordre aléatoire des couleurs.
     *
     * @return Les couleurs.
     */
    List<CouleurCarte> getCouleurs();
}
