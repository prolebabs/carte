package com.carte.record;

import com.carte.enums.CouleurCarte;

public record Carte(CouleurCarte couleur, int valeur) {

}