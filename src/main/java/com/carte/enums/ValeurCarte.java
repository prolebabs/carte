package com.carte.enums;

public enum ValeurCarte {
    AS(14), CINQ(5), DIX(10), HUIT(8), SIX(6), SEPT(7), QUATRE(4),

    DEUX(2), TROIS(3), NEUF(9), DAME(12), ROI(13), VALET(11);

    private final int valeur;

    ValeurCarte(int valeur) {
        this.valeur = valeur;
    }

    public int getValeur() {
        return valeur;
    }
}
