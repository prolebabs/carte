package com.carte.enums;

public enum CouleurCarte {
    Carreaux, Coeur, Pique, Trefle
}
