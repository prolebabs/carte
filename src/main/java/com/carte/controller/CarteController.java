package com.carte.controller;

import com.carte.record.Carte;
import com.carte.service.ICarteService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/carte")
@CrossOrigin(origins = "http://localhost:4200")
public class CarteController {
    private final ICarteService carteService;

    public CarteController(ICarteService carteService) {
        this.carteService = carteService;
    }

    @GetMapping
    public ResponseEntity<List<Carte>> getCartes() {
        return ResponseEntity.ok(carteService.tirerMain());
    }

    @PostMapping
    public ResponseEntity<List<Carte>> trierCartes(@RequestBody List<Carte> cartes) {
        return ResponseEntity.ok(carteService.trierMain(cartes));
    }

}
